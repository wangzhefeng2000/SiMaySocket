﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Sockets.Tcp
{
    public interface ITcpSocketSaeaServerConfiguration:ITcpSocketSaeaConfiguration
    {
        bool AppKeepAlive { get; set; }
        int PendingConnectionBacklog { get; set; }
    }
}
