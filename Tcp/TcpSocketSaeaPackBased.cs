﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using SiMay.Sockets.Delegate;
using SiMay.Sockets.Tcp.Awaitable;
using SiMay.Sockets.Tcp.Client;
using SiMay.Sockets.Tcp.Pooling;
using SiMay.Sockets.UtilityHelper;

namespace SiMay.Sockets.Tcp
{
    public class TcpSocketSaeaPackBased : TcpSocketSaeaSession
    {
        private readonly byte[] _emptyHeart = new byte[] { 0, 0, 0, 0 };
        private int _packageRecvOffset;
        private bool _iscompress;
        private byte[] _headbuffer = new byte[4];
        private byte[] _packbuffer;

        internal int _isuchannel = 0;
        internal DateTime _heartTime { get; private set; } = DateTime.Now;
        internal TcpSocketSaeaPackBased(
            TcpSocketConfiguration configuration,
            SaeaAwaiterPool handlerSaeaPool,
            SessionPool sessionPool,
            NotifyEventHandler<TcpSocketCompletionNotify, TcpSocketSaeaSession> notifyEventHandler,
            LogHelper logHelper,
            TcpSocketSaeaEngineBased agent)
            : base(notifyEventHandler, configuration, handlerSaeaPool, sessionPool, agent, logHelper)
        {
            _iscompress = configuration.CompressBuffer;
        }

        internal override void Attach(Socket socket)
        {
            if (socket == null)
                throw new ArgumentNullException("socket");

            lock (_opsLock)
            {
                this._heartTime = DateTime.Now;
                this._state = TcpSocketConnectionState.Connected;
                this._socket = socket;
                this._startTime = DateTime.UtcNow;
                this.SetSocketOptions();
            }
        }

        internal override void Detach()
        {
            lock (_opsLock)
            {
                this._socket = null;
                this._state = TcpSocketConnectionState.None;
                this.AppTokens = null;
                this._packageRecvOffset = 0;
            }
        }

        private void SetSocketOptions()
        {
            _socket.ReceiveBufferSize = _configuration.ReceiveBufferSize;
            _socket.SendBufferSize = _configuration.SendBufferSize;
            _socket.ReceiveTimeout = (int)_configuration.ReceiveTimeout.TotalMilliseconds;
            _socket.SendTimeout = (int)_configuration.SendTimeout.TotalMilliseconds;
            _socket.NoDelay = _configuration.NoDelay;

            if (_configuration.KeepAlive)
                SetKeepAlive(_socket, 1, _configuration.KeepAliveInterval, _configuration.KeepAliveSpanTime);

            _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, _configuration.ReuseAddress);
        }

        private void SetKeepAlive(Socket sock,byte op,int interval,int spantime)
        {
            uint dummy = 0;
            byte[] inOptionValues = new byte[Marshal.SizeOf(dummy) * 3];
            BitConverter.GetBytes(op).CopyTo(inOptionValues, 0);//开启keepalive
            BitConverter.GetBytes((uint)interval).CopyTo(inOptionValues, Marshal.SizeOf(dummy));//多长时间开始第一次探测
            BitConverter.GetBytes((uint)spantime).CopyTo(inOptionValues, Marshal.SizeOf(dummy) * 2);//探测时间间隔
            sock.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);
        }

        internal override void StartProcess()
        {
            var awaiter = _handlerSaeaPool.Take();

            //if (awaiter == null)
            //{
            //    _logger.WriteLog("session startprocess awaiter null count:" + _handlerSaeaPool.Count);
            //    awaiter = new SaeaAwaiter();
            //}

            awaiter.Saea.SetBuffer(_headbuffer, _packageRecvOffset, _headbuffer.Length);
            SaeaExHelper.ReceiveAsync(_socket, awaiter, HeadProcess);
        }

        private void HeadProcess(SaeaAwaiter awaiter, SocketError error)
        {
            if (awaiter.Saea.BytesTransferred == 0 ||
                error != SocketError.Success ||
                _state != TcpSocketConnectionState.Connected ||
                _socket == null)
            {
                _logger.WriteLog("session_recv endtransfer state：" + _state.ToString() + " socket_error：" + error.ToString());
                this.EndTransfer(awaiter);
                return;
            }

            _heartTime = DateTime.Now;
            _packageRecvOffset += awaiter.Saea.BytesTransferred;
            if (_packageRecvOffset >= 4)
            {
                int packLength = BitConverter.ToInt32(_headbuffer, 0);

                if (packLength < 0 || packLength > _configuration.SendBufferSize)//长度包越界判断
                {
                    this.Close(true);
                    return;
                }

                if (packLength == 0)
                {
                    if (this._configuration._isserviceMode) //如果是服务端，则反馈心跳包
                        if (this._isuchannel == 0)
                        {
                            var h_saea = _handlerSaeaPool.Take();

                            //if (h_saea == null)
                            //{
                            //    _logger.WriteLog("session h_thread send awaiter null pool count:" + _handlerSaeaPool.Count);
                            //    h_saea = new SaeaAwaiter();
                            //}

                            //4个字节空包头
                            h_saea.Saea.SetBuffer(_emptyHeart, 0, _emptyHeart.Length);
                            SaeaExHelper.SendAsync(this._socket, h_saea, (a, e) => _handlerSaeaPool.Return(a));
                        }

                    //继续接收包头
                    _packageRecvOffset = 0;
                    awaiter.Saea.SetBuffer(_packageRecvOffset, _headbuffer.Length);
                    SaeaExHelper.ReceiveAsync(this._socket, awaiter, HeadProcess);
                    return;
                }
                _packbuffer = new byte[packLength];
                _packageRecvOffset = 0;

                awaiter.Saea.SetBuffer(_packbuffer, _packageRecvOffset, _packbuffer.Length);
                SaeaExHelper.ReceiveAsync(this._socket, awaiter, RecvtingProcess);
            }
            else
            {
                awaiter.Saea.SetBuffer(_packageRecvOffset, _headbuffer.Length - _packageRecvOffset);
                SaeaExHelper.ReceiveAsync(this._socket, awaiter, HeadProcess);
            }
        }
        private void RecvtingProcess(SaeaAwaiter awaiter, SocketError error)
        {
            int BytesTransferred = awaiter.Saea.BytesTransferred;
            if (BytesTransferred == 0 ||
                error != SocketError.Success ||
                _state != TcpSocketConnectionState.Connected ||
                _socket == null)
            {
                _logger.WriteLog("session_recv endtransfer state：" + _state.ToString() + " socket_error：" + error.ToString());
                this.EndTransfer(awaiter);
                return;
            }

            this.ReceiveTransferredBytes = BytesTransferred;
            this._notifyEventHandler?.Invoke(TcpSocketCompletionNotify.OnDataReceiveing, this);

            _heartTime = DateTime.Now;
            _packageRecvOffset += BytesTransferred;
            if (_packageRecvOffset >= _packbuffer.Length)
            {
                this.PackageProcess();

                _packageRecvOffset = 0;
                awaiter.Saea.SetBuffer(_headbuffer, _packageRecvOffset, _headbuffer.Length);
                SaeaExHelper.ReceiveAsync(this._socket, awaiter, HeadProcess);
            }
            else
            {
                awaiter.Saea.SetBuffer(_packageRecvOffset, _packbuffer.Length - _packageRecvOffset);
                SaeaExHelper.ReceiveAsync(this._socket, awaiter, RecvtingProcess);
            }
        }

        private void PackageProcess()
        {
            if (this._iscompress)
            {
                _completebuffer = CompressHelper.Decompress(_packbuffer);
            }
            else
            {
                _completebuffer = _packbuffer;
            }

            this._notifyEventHandler?.Invoke(TcpSocketCompletionNotify.OnDataReceived, this);
        }

        private void EndTransfer(Awaitable.SaeaAwaiter awaiter)
        {
            this.Close(true);
            this._handlerSaeaPool.Return(awaiter);
            this._sessionPool.Return(this);
        }

        public override void SendAsync(byte[] data)
        {
            this.SendAsync(data, 0, data.Length);
        }

        public override void SendAsync(byte[] data, int offset, int lenght)
        {

            if (this._socket == null)
                return;

            byte[] buff;
            byte[] lenBytes;

            if (this._iscompress)
            {
                byte[] compressedBytes = CompressHelper.Compress(data, offset, lenght);

                buff = new byte[compressedBytes.Length + 4];
                lenBytes = BitConverter.GetBytes(compressedBytes.Length);
                lenBytes.CopyTo(buff, 0);
                compressedBytes.CopyTo(buff, 4);
            }
            else
            {
                buff = new byte[lenght + 4];

                lenBytes = BitConverter.GetBytes(lenght);
                lenBytes.CopyTo(buff, 0);
                Array.Copy(data, offset, buff, 4, buff.Length);
            }

            var awaiter = _handlerSaeaPool.Take();

            //if (awaiter == null)
            //{
            //    _logger.WriteLog("session send awaiter null pool count:" + _handlerSaeaPool.Count);
            //    awaiter = new SaeaAwaiter();
            //}

            awaiter.Saea.SetBuffer(buff, 0, buff.Length);
            Interlocked.Exchange(ref _isuchannel, 1);
            SaeaExHelper.SendAsync(this._socket, awaiter, (a, e) =>
             {
                 this.SendTransferredBytes = a.Saea.Buffer.Length;

                 Interlocked.Exchange(ref _isuchannel, 0);
                 _handlerSaeaPool.Return(awaiter);
                 this._notifyEventHandler?.Invoke(TcpSocketCompletionNotify.OnSend, this);
             });

        }

        public override void Close(bool notify)
        {
            lock (_opsLock)
            {
                if (_socket != null)
                {
                    try
                    {
                        _socket.Shutdown(SocketShutdown.Both);
                        _socket.Close();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteLog("session_close info：" + e.Message);
                    }
                    finally
                    {
                        _socket = null;
                    }
                    _state = TcpSocketConnectionState.Closed;

                    if (notify)
                    {
                        this._notifyEventHandler?.Invoke(TcpSocketCompletionNotify.OnClosed, this);
                    }
                }
            }
        }
    }
}
