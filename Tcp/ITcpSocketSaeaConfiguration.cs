﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Sockets.Tcp
{
    public interface ITcpSocketSaeaConfiguration
    {
        bool CompressBuffer { get; set; }
        int ReceiveBufferSize { get; set; }
        int SendBufferSize { get; set; }
        TimeSpan ReceiveTimeout { get; set; }
        TimeSpan SendTimeout { get; set; }
        bool NoDelay { get; set; }
        bool KeepAlive { get; set; }
        int KeepAliveInterval { get; set; }
        int KeepAliveSpanTime { get; set; }
        bool ReuseAddress { get; set; }
    }
}
