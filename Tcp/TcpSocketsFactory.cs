﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using SiMay.Sockets.Delegate;
using SiMay.Sockets.Tcp.Client;
using SiMay.Sockets.Tcp.Server;

namespace SiMay.Sockets.Tcp
{
    public class TcpSocketsFactory
    {
        public static TcpSocketSaeaClientAgent CreateClientAgent(
            TcpSocketSaeaSessionType saeaSessionType,
            ITcpSocketSaeaClientConfiguration configuration,
            NotifyEventHandler<TcpSocketCompletionNotify, TcpSocketSaeaSession> completetionNotify)
        {
            TcpSocketConfiguration config = (TcpSocketConfiguration)configuration ?? new TcpSocketConfiguration();
            config._isserviceMode = false;

            return new TcpSocketSaeaClientAgent(saeaSessionType, config, completetionNotify);
        }

        public static TcpSocketSaeaServer CreateServerAgent(
            TcpSocketSaeaSessionType saeaSessionType,
            ITcpSocketSaeaServerConfiguration configuration,
            NotifyEventHandler<TcpSocketCompletionNotify, TcpSocketSaeaSession> completetionNotify)
        {
            TcpSocketConfiguration config = (TcpSocketConfiguration)configuration ?? new TcpSocketConfiguration();
            config._isserviceMode = true;

            return new TcpSocketSaeaServer(saeaSessionType, config, completetionNotify);
        }
    }
}
